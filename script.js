import { createAddStudentPopup } from "./addStudentPopup.js";


// object global dataStudent
export var dataStudent = [
    {
        code: "001",
        name: "Nguyen Van A",
        email: "nva@gmail.com",
        phone: "0123456789",
        dob: "01/01/1990",
        gender: "Nam",
        department: "Điện tử viễn thông",
        clazz: "DHCNTT13A"
    },
    {
        code: "002",
        name: "Tran Thi B",
        email: "ttb@gmail.com",
        phone: "0987654321",
        dob: "02/02/2004",
        gender: "Nữ",
        department: "Công nghệ thông tin",
        clazz: "DHCNTT13B"
    },
    {
        code: "003",
        name: "Le Van C",
        email: "lvc@gmail.com",
        phone: "0123987456",
        dob: "03/03/2003",
        gender: "Nam",
        department: "Công nghệ thông tin",
        clazz: "DHCNTT13C"
    },
    {
        code: "004",
        name: "Pham Thi D",
        email: "ptd@gmail.com",
        phone: "0912345678",
        dob: "04/04/1993",
        gender: "Nữ",
        department: "Công nghệ thông tin",
        clazz: "DHCNTT13D"
    },
    {
        code: "005",
        name: "Hoang Van E",
        email: "hve@gmail.com",
        phone: "0987654321",
        dob: "05/05/2002",
        gender: "Nam",
        department: "Điện tử viễn thông",
        clazz: "DHCNTT13E"
    },
    {
        code: "006",
        name: "Nguyen Thi F",
        email: "ntf@gmail.com",
        phone: "0123456780",
        dob: "06/06/2001",
        gender: "Nữ",
        department: "Điện tử viễn thông",
        clazz: "DHCNTT13F"
    },
    {
        code: "007",
        name: "Tran Van G",
        email: "tvg@gmail.com",
        phone: "0987654320",
        dob: "07/07/2002",
        gender: "Nam",
        department: "Điện tử viễn thông",
        clazz: "DHCNTT13G"
    },
    {
        code: "008",
        name: "Le Thi H",
        email: "lth@gmail.com",
        phone: "0123456790",
        dob: "08/08/1997",
        gender: "Nữ",
        department: "Điện tử viễn thông",
        clazz: "DHCNTT13H"
    },
    {
        code: "009",
        name: "Pham Van I",
        email: "pvi@gmail.com",
        phone: "0912345789",
        dob: "09/09/1998",
        gender: "Nam",
        department: "Cơ khí",
        clazz: "DHCNTT13I"
    },
    {
        code: "010",
        name: "Hoang Thi J",
        email: "htj@gmail.com",
        phone: "0987654312",
        dob: "10/10/1999",
        gender: "Nữ",
        department: "Cơ khí",
        clazz: "DHCNTT13J"
    },
    {
        code: "011",
        name: "Nguyen Van K",
        email: "nvk@gmail.com",
        phone: "0123456809",
        dob: "11/11/2011",
        gender: "Nam",
        department: "Cơ khí",
        clazz: "DHCNTT13K"
    },
];


// data for search, filter, sort 
export var tempData = [...dataStudent];

// render table form dataStudent
export function renderTable(data, page) {
    let table = document.getElementById("studentTable");
    let tbody = table.getElementsByTagName("tbody")[0];

    // clear table
    tbody.innerHTML = "";

    // render table
    for (let i = page * 10; i < page * 10 + 10; i++) {
        let row = data[i];
        if(!row) break;

        let trClass = "border-b hover:bg-alice-blue";
        let tdClass = "pt-4 pb-4 px-4 text-center font-normal font-helvetica text-gray-gray-700";
        let tr = document.createElement("tr");
        tr.className = trClass;
        for (let key in row) {
            let td = document.createElement("td");
            td.className = tdClass;
            td.textContent = row[key];
            tr.appendChild(td);
        }
        let updateBtn = createUpdateBtn();
        let deleteBtn = createDeleteBtn();

        // create td for update button
        let td = document.createElement("td");
        td.className = "text-center";
        td.appendChild(updateBtn);
        td.appendChild(deleteBtn);

        tr.appendChild(td);
        updateBtn.addEventListener("click", function() {
            createAddStudentPopup(row.code);
        });

        deleteBtn.addEventListener("click", function() {
            deleteStudent(row.code);
        });
        
        // append tr to tbody
        tbody.appendChild(tr);
    }
}


// delete student
function deleteStudent(code) {
    let index = dataStudent.findIndex(item => item.code === code);
    dataStudent.splice(index, 1);
    tempData = [...dataStudent];
    setPagination(parseInt((tempData.length - 1) / 10) + 1);
    renderTable(dataStudent, 0);
}

// create update button
function createUpdateBtn() {
    let updateBtn = document.createElement("button");
    updateBtn.className = "bg-blue-500 hover:bg-blue-700 text-white font-bold text-[12px] py-2 px-4 rounded";
    updateBtn.textContent = "Sửa";
    
    return updateBtn;
}

function createDeleteBtn() {
    let deleteBtn = document.createElement("button");
    deleteBtn.className = "bg-red-500 hover:bg-red-700 text-white font-bold text-[12px] py-2 px-4 ml-[10px] rounded";
    deleteBtn.textContent = "Xóa";

    return deleteBtn;
}

// render table when document loaded
document.addEventListener("DOMContentLoaded", function() {
    renderTable(dataStudent, 0);

    let addStudentBtn = document.getElementById("addStudent");
    addStudentBtn.removeEventListener("click", function() {});
    addStudentBtn.addEventListener("click", function() {
        createAddStudentPopup();
    });
});


// search student
document.getElementById("search-btn").addEventListener("click", function() {
    let keyword = document.getElementById("search-input").value.trim();
    tempData = dataStudent.filter(item => {
        return item.name.toLowerCase().includes(keyword.toLowerCase());
    });
    renderTable(tempData, 0);
    setPagination(parseInt((tempData.length - 1) / 10) + 1);
});


// filter student
function setOption(data, id, key) {
    let select = document.getElementById(id);
    let options = data.map(item => item[key]);
    options = [...new Set(options)];

    for(let option of options) {
        let opt = document.createElement("option");
        opt.value = option;
        opt.textContent = option;
        select.appendChild(opt);
    }
}

document.addEventListener("DOMContentLoaded", function() {
    setOption(dataStudent, "filter-department", "department");
    setOption(dataStudent, "filter-clazz", "clazz");
    // setOption(dataStudent, "filter-birth-year", "dob");
});

document.getElementById("filter-btn").addEventListener("click", function() {
    let department = document.getElementById("filter-department");
    let clazz = document.getElementById("filter-clazz");
    let birthYear = document.getElementById("filter-birth-year");
    let departmentItem = department.options[department.selectedIndex];
    let clazzItem = clazz.options[clazz.selectedIndex];
    let birthYearItem = birthYear.options[birthYear.selectedIndex];
    
    let result = tempData.length > 0 ? tempData : dataStudent;
    result = filterByDepartment(result, departmentItem.value);
    result = filterByClazz(result, clazzItem.value);
    result = filterByBirthYear(result, birthYearItem.value);
    renderTable(result, 0);
    setPagination(parseInt((result.length - 1)/ 10) + 1);
});

document.getElementById("reset-btn").addEventListener("click", function() {
    tempData = [...dataStudent];
    renderTable(tempData, 0);
    document.getElementById("filter-department").selectedIndex = 0;
    document.getElementById("filter-clazz").selectedIndex = 0;
    document.getElementById("filter-birth-year").selectedIndex = 0;
})

function filterByDepartment(data, department) {
    if(department === "000") return data;
    return data.filter(item => item.department === department);
}

function filterByClazz(data, clazz) {
    if(clazz === "000") return data;
    return data.filter(item => item.clazz === clazz);
}

function filterByBirthYear(data, birthYear) {
    let prevBirthYear = birthYear.split("-")[0];
    let nextBirthYear = birthYear.split("-")[1];

    if(nextBirthYear === "now") nextBirthYear = new Date().getFullYear();
    if(birthYear === "000") return data;
    return data.filter(item => {
        let year = item.dob.split("/")[2];
        return year >= prevBirthYear && year <= nextBirthYear;
    });
}

// pagination
let pageCount = parseInt((dataStudent.length - 1) / 10) + 1 ;
let currentPage = 1;

document.addEventListener("DOMContentLoaded", function() {
    setPagination(pageCount);
})

function setPagination(pageCount) {
    createPagination(pageCount);
    let prevBtn = document.getElementById("prev-btn");
    let nextBtn = document.getElementById("next-btn");

    if(pageCount === 1) {
        nextBtn.disabled = true;
        prevBtn.disabled = true;
    }

    prevBtn.addEventListener("click", function() {
        currentPage--;
        if(currentPage >= 1) {
            renderTable(tempData, currentPage - 1);
        }
        setActiveButton();
    });

    nextBtn.addEventListener("click", function() {
        currentPage++;
        if(currentPage <= pageCount){
            renderTable(tempData, currentPage - 1);
        }
        setActiveButton();
    });
}


function createPagination(pageCount) {
    let pagination = document.getElementById("pagination");
    pagination.innerHTML = "";
    for(let i = 1; i <= pageCount; i++) {
        let page = document.createElement("button");
        page.className = "font-helvetica hover:bg-blue-700 hover:text-white text-black font-bold text-[14px] py-2 px-4 rounded";
        page.textContent = i;
        if(i === 1) page.classList.add("bg-blue-500", "text-white");
        page.addEventListener("click", function() {
            renderTable(tempData, i - 1);
            currentPage = i;
            setActiveButton();
        });
        pagination.appendChild(page);
    }
}

function setActiveButton() {
    let prevBtn = document.getElementById("prev-btn");
    let nextBtn = document.getElementById("next-btn");

    // check button disabled
    if(currentPage < 1) {
        prevBtn.disabled = true;
        currentPage = 1;
        return;
    }
    else
        prevBtn.disabled = false;

    if(currentPage > pageCount) {
        nextBtn.disabled = true;
        currentPage = pageCount;
        return;
    } 
    else nextBtn.disabled = false;

    // set active button
    let pages = document.getElementById("pagination").children;
    for(let page of pages) {
        page.classList.remove("bg-blue-500", "text-white");
    }
    pages[currentPage - 1].classList.add("bg-blue-500", "text-white");
}


// sorting 
let tempSortData = [...tempData];
let keyArr = Object.keys(dataStudent[0]);
let ths = document.getElementById("studentTable").getElementsByTagName("th");

ths = [...ths];
ths.pop(); // remove update and delete th
ths.forEach((th, index) => {
    th.addEventListener("click", function() {
        let classList = [...th.classList];

        let order = classList.find(item => item.includes("sorting"));
        
        if(order == "sorting") order = "_asc";
        else if(order === "sorting_asc") order = "_desc";
        else order = "";        
        
        resetSort();
        th.classList.remove("sorting");
        th.classList.add("sorting" + order);
        let key = keyArr[ths.indexOf(th)];
        sortAll(tempSortData, key, order);
        renderTable(tempSortData, 0);
    });
});

function resetSort() {
    ths.forEach(th => {
        th.classList.remove("sorting_asc", "sorting_desc", "sorting");
    });
    ths.forEach(th => {
        th.classList.add("sorting");
    });
}

function sortAll(data, key, order) {
    data = [...tempData];
    if(order === "_asc") {
        if(key === "dob") {
            sortData(data, (a, b) => {
                let aDate = new Date(a[key].split("/").reverse().join("/"));
                let bDate = new Date(b[key].split("/").reverse().join("/"));
                return aDate - bDate;
            });
            return;
        } 
        sortData(data, (a, b) => a[key].localeCompare(b[key]))
    } else if (order === "_desc"){
        if(key === "dob") {
            sortData(data, (a, b) => {
                let aDate = new Date(a[key].split("/").reverse().join("/"));
                let bDate = new Date(b[key].split("/").reverse().join("/"));
                return bDate - aDate;
            });
            return;
        }
        sortData(data, (a, b) => b[key].localeCompare(a[key]))
    } else {
        tempSortData = [...tempData];
        renderTable(tempSortData, 0);
    }
}

function sortData(data, compare) {
    // selection sort
    for(let i = 0; i < data.length - 1; i++){
        let min = i;
        for(let j = i + 1; j < data.length; j++){
            if(compare(data[j], data[min]) < 0){
                min = j;
            }
        }
        let tmp = data[min];
        data[min] = data[i];
        data[i] = tmp;
    }
    
    tempSortData = data;
    renderTable(tempSortData, 0);
}
