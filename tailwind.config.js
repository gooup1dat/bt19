/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './*.js',
  ],
  theme: {
    extend: {
      colors: {
        'gray-gray-700': 'var(--Gray-Gray-700, #2D3748)',
        'gray-gray-500': 'var(--Gray-Gray-500, #718096)',
        'gray-gray-400': 'var(--Gray-Gray-400, #A0AEC0)',
      },
      borderRadius : {
        'c-12': '12px',
        'c-15': '15px',
      },
      gap: {
        'c-18': '18px',
      },
    
      borderColor: {
        'gray-gray-200': 'var(--Gray-Gray-200, #E2E8F0)',
      },
      fontFamily: {
        helvetica: 'Helvetica, Arial, sans-serif',
        'helvetica-neue': 'Helvetica Neue, Arial, sans-serif',
      },
      backgroundColor: {
        "Ghost-White" : "#F8F9FA",
        'teal-teal-300': 'var(--Teal-Teal-300, #4FD1C5)',
        'alice-blue': '#ebf4ff',
      },
      boxShadow : {
        'c-icon' : '0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)',
      }
    },
  },
  plugins: [],
}

